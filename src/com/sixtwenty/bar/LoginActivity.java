package com.sixtwenty.bar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.sixtwenty.bar.ConsumerOrders.GetSingleInvoice;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class LoginActivity extends SherlockActivity {

	ActionBar actionBar;
	Button btnlogin;
	EditText txtUser, txtPass;
	PopupWindow mypop;
	AlertDialogManager alert = new AlertDialogManager();
	JSONParser jsonParser = new JSONParser();
	private ProgressDialog pDialog;
	String username, password, welcome_name, session_id, level;
	private static final String get_user = "http://10.0.2.2/vc/chapaa/get_user.php";
	private static final String get_user2 = "http://www.padawan.site40.net/chapaa/get_user.php";

	// products JSONArray
	JSONArray user = null;

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_USER = "user";
	private static final String TAG_PID = "uid";
	private static final String TAG_NAME = "uname";
	private static final String TAG_LEVEL = "ulevel";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		txtUser = (EditText) findViewById(R.id.userText);
		txtPass = (EditText) findViewById(R.id.passText);
		btnlogin = (Button) findViewById(R.id.btnLogin);

		btnlogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get user name/number, password from EditText
				username = txtUser.getText().toString();
				password = txtPass.getText().toString();

				// Check if user name, password is filled
				if (username.trim().length() > 0
						&& password.trim().length() > 0) {

					new LoginUser().execute();

				} else {
					// user didn't enter username or password
					// Show alert asking him to enter the details
					alert.showAlertDialog(LoginActivity.this, "Login Failed.",
							"Please enter Phone Number and Password", false);
				}

			}
		});

		actionBar = getSupportActionBar();
		actionBar.hide();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.activity_login, menu);
		return true;
	}
	

	/**
	 * Background Async Task to Get complete student details
	 * */
	class LoginUser extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Authenticating...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Getting student details in background thread
		 * */
		protected String doInBackground(String... args) {

			try {
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("user_number", username));
				params.add(new BasicNameValuePair("password", password));

				// getting product details by making HTTP request
				// Note that product details url will use GET request
				JSONObject json = jsonParser.makeHttpRequest(get_user2, "GET",
						params);

				// check your log for json response
				Log.d("User Details", json.toString());

				// json success tag
				int success;

				success = json.getInt(TAG_SUCCESS);
				if (success == 1) {
					// products found
					// Getting Array of Products
					user = json.getJSONArray(TAG_USER);

					// looping through All Products
					for (int i = 0; i < user.length(); i++) {
						JSONObject c = user.getJSONObject(i);

						// Storing each json item in variable
						session_id = c.getString(TAG_PID);
						welcome_name = c.getString(TAG_NAME);
						level = c.getString(TAG_LEVEL);
						
						

						runOnUiThread(new Runnable() {
							public void run() {

								Toast.makeText(
										getApplicationContext(),
										"Welcome " + welcome_name
												+ " Session ID " + session_id + "Level" + level,
										Toast.LENGTH_SHORT).show();
							}
						});
						
					
							Intent in = new Intent(getApplicationContext(),
									MyOrders.class);

							in.putExtra("uname", welcome_name);
							in.putExtra("waiter_id", session_id);

							startActivity(in);
							finish();

						

						

					}

				} else {
					// user not found
					runOnUiThread(new Runnable() {
						public void run() {

							alert.showAlertDialog(LoginActivity.this,
									"Login Failed.",
									"Username/Password is incorrect", false);
						}
					});

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();

		}
	}

	/**
	 * Background Async Task to Get complete student details
	 * */
	class GetUser extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Authenticating. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Getting student details in background thread
		 * */
		protected String doInBackground(String... params) {

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					// Check for success tag
					int success;
					try {
						// Building Parameters
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("user_number",
								username));
						params.add(new BasicNameValuePair("password", password));

						// getting product details by making HTTP request
						// Note that product details url will use GET request
						JSONObject json = jsonParser.makeHttpRequest(get_user2,
								"GET", params);

						// check your log for json response
						Log.d("User Details", json.toString());

						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// products found
							// Getting Array of Products
							user = json.getJSONArray(TAG_USER);

							// looping through All Products
							for (int i = 0; i < user.length(); i++) {
								JSONObject c = user.getJSONObject(i);

								// Storing each json item in variable
								session_id = c.getString(TAG_PID);
								welcome_name = c.getString(TAG_NAME);
								String number = c.getString(TAG_LEVEL);

							}

						} else {
							// user not found
							alert.showAlertDialog(LoginActivity.this,
									"Login Failed.",
									"Username/Password is incorrect", false);

						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			});

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all detailss
			pDialog.dismiss();

			Toast.makeText(getApplicationContext(), "Welcome " + welcome_name,
					Toast.LENGTH_SHORT).show();
			// toast.show();
			// Staring MainActivity
			Intent in = new Intent(getApplicationContext(), HomeActivity.class);
			startActivity(in);
			finish();
		}
	}

}
