package com.sixtwenty.bar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockListActivity;

public class ConsumerOrders extends SherlockListActivity {

	ActionBar actionBar;
	String con_invoice_num, invo;
	JSONParser jsonParser = new JSONParser();
	private ProgressDialog pDialog;
	ArrayList<HashMap<String, String>> invoiceList;
	private static final String get_invoice_details = "http://10.0.2.2/vc/chapaa/get_single_invoice.php";
	private static final String get_invoice_details2 = "http://www.padawan.site40.net/chapaa/get_single_invoice.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCT_NAME = "product_name";
	private static final String TAG_PRODUCTS = "products";
	private static final String TAG_PID = "pid";
	private static final String TAG_INVOICE = "invoice";
	private static final String TAG_INVOICE_DATE = "invoice_date";
	private static final String TAG_QUANTITY = "quantity";
	private static final String TAG_NUMBER = "table_number";
	private static final String TAG_PRICE = "price";
	private static final String TAG_STATUS = "status";
	
	// products JSONArray
		JSONArray products = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			invo = extras.getString("theinvoice");

		}
		
		con_invoice_num = "Customer Order No.";

		setTitle(con_invoice_num + ":" +invo);
		//setTitle(con_invoice_num);
		setContentView(R.layout.single_invoice);
		

		// Hashmap for ListView
		invoiceList = new ArrayList<HashMap<String, String>>();
		// Getting single invoice details in background thread
		new GetSingleInvoice().execute();

		// Initializing Action Bar
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#2E9AFE")));

	}

	/**
	 * Background Async Task to Get complete student details
	 * */
	class GetSingleInvoice extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ConsumerOrders.this);
			pDialog.setMessage("Loading Invoice. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Getting student details in background thread
		 * */
		protected String doInBackground(String... args) {
					
					try {
						// Building Parameters
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						params.add(new BasicNameValuePair("invoice_no",
								invo));

						// getting product details by making HTTP request
						// Note that product details url will use GET request
						JSONObject json = jsonParser.makeHttpRequest(
								get_invoice_details2, "GET", params);

						// check your log for json response
						Log.d("Single Invoice Details", json.toString());

						int success;
						// json success tag
						success = json.getInt(TAG_SUCCESS);
						if (success == 1) {
							// products found
							// Getting Array of Products
							products = json.getJSONArray(TAG_PRODUCTS);

							// looping through All Products
							for (int i = 0; i < products.length(); i++) {
								JSONObject c = products.getJSONObject(i);

								// Storing each json item in variable
								String id = c.getString(TAG_PID);
								String product_name = c.getString(TAG_PRODUCT_NAME);
								String status = c.getString(TAG_STATUS);
								String number = c.getString(TAG_NUMBER);
								String invoice_no = c.getString(TAG_INVOICE);
								String amount = c.getString(TAG_PRICE);
								String quantity = c.getString(TAG_QUANTITY);

								// creating new HashMap
								HashMap<String, String> map = new HashMap<String, String>();

								// adding each child node to HashMap key => value
								map.put(TAG_PID, id);
								map.put(TAG_PRODUCT_NAME, product_name);
								map.put(TAG_STATUS, status);
								map.put(TAG_NUMBER, number);
								map.put(TAG_INVOICE, invoice_no);
								map.put(TAG_PRICE, amount);
								map.put(TAG_QUANTITY, quantity);

								// adding HashList to ArrayList
								invoiceList.add(map);
							}

						} else {
							// product with pid not found
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
			

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once got all details
			pDialog.dismiss();

			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					ListAdapter adapter = new SimpleAdapter(
							ConsumerOrders.this, invoiceList,
							R.layout.consumer_invoice, new String[] {
									TAG_PID, TAG_PRODUCT_NAME, TAG_PRICE,
									TAG_QUANTITY, TAG_NUMBER }, new int[] {
									R.id.pid, R.id.actualRef,
									R.id.actualDate, R.id.actualQuantity,
									R.id.actualNumber });
					// updating listview
					setListAdapter(adapter);

				}
			});
		}
	}

}
