package com.hewani.fragments;

import com.sixtwenty.bar.InvoiceConsumer;
import com.sixtwenty.bar.PayBillTransactions;
import com.sixtwenty.bar.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class HewaniMenuFragment extends ListFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		String[] mainMenu = getResources().getStringArray(
				R.array.hewani_menu_array);
		
		ArrayAdapter<String> MyMenuAdapter = new ArrayAdapter<String>(
				getActivity(), R.layout.main_menu_list,
				R.id.menuItem, mainMenu);
		setListAdapter(MyMenuAdapter);
		/*
		ArrayAdapter<String> colorAdapter = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_list_item_1,
				android.R.id.text1, colors);
		setListAdapter(colorAdapter);*/
	}

	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {
		switch (position) {
		case 0:
			Intent i = new Intent(getActivity().getApplicationContext(),
					InvoiceConsumer.class);
			startActivity(i);
			//newContent = new ColorFragment(R.color.red);
			break;
		case 1:
			Intent in = new Intent(getActivity().getApplicationContext(),
					PayBillTransactions.class);
			startActivity(in);
			break;
		case 2:
			
			
			break;
		
		}

	}

}
