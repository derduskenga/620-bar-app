package com.sixtwenty.bar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;

public class InvoiceSent extends SherlockActivity {

	ActionBar actionBar;
	Button btnViewInvoice, btnInvoiceCustomer, btnPaybill;
	TextView txtOrderRef, txtPhoneNumber;
	String inv_number, ph_number;
	TextView orderRef, customerPhone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header_invoice_sent);
		setContentView(R.layout.invoice_sent);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			inv_number = extras.getString("invoice");
			ph_number = extras.getString("number");

		}
		
		orderRef = (TextView) findViewById(R.id.txtRefNumber);
		customerPhone = (TextView) findViewById(R.id.txtPhoneNumber);
		
		orderRef.setText(inv_number);
		customerPhone.setText(extras.getString("number"));
		

		btnViewInvoice = (Button) findViewById(R.id.btnViewInvoice);
		btnInvoiceCustomer = (Button) findViewById(R.id.btnInvoiceCustomer);
		btnPaybill = (Button) findViewById(R.id.btnViewPaybill);
		
		btnViewInvoice.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		btnPaybill.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		btnInvoiceCustomer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(), InvoiceConsumer.class);
                startActivity(i);

			}
		});
		
		btnPaybill.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(), PayBillTransactions.class);
                startActivity(i);

			}
		});

		txtOrderRef = (TextView) findViewById(R.id.txtOrderRef);
		txtPhoneNumber = (TextView) findViewById(R.id.txtPhoneNumber);

		
		// Initializing Action Bar
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#69BD28")));

	}

}
