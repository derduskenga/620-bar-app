package com.sixtwenty.bar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView.Validator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.hewani.handlers.DatabaseConnector;

public class AddProduct extends SherlockActivity {

	ActionBar actionBar;
	String dt;
	Date mydate;
	String invoice_no = "AAA123", inv, nmbr, waiter_id;
	// get products url
	private static String url_get_products = "http://10.0.2.2/vc/chapaa/get_products.php";
	private static String url_get_products2 = "http://www.padawan.site40.net/chapaa/get_products.php";
	// post to invoice
	private static String url_add_product_to_invoice = "http://10.0.2.2/vc/chapaa/add_product.php";

	private static String url_get_price = "http://www.padawan.site40.net/chapaa/get_price.php";
	private static String url_add_product_to_invoice2 = "http://www.padawan.site40.net/chapaa/add_product.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCTS = "products";
	private static final String TAG_PID = "pid";
	private static final String TAG_NAME = "name";
	private static final String TAG_PRICE = "price";
	private static final String THE_PRICE = "theprice";
	private static final String TAG_PRICES = "product_price";

	// products JSONArray
	JSONArray products = null;
	JSONArray prices = null;
	private ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	Spinner spinner, priceSpinner;
	EditText txtPrice, txtProduct, txtTableNumber, txtQuantity, txtTotalPrice;
	Button btngetPrice;
	String theProductName, product_price;
	Long theProductID;
	double x = 0;
	double y = 0;
	double z = 0;

	ArrayList<HashMap<String, String>> productsList;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header_add_product);
		setContentView(R.layout.add_product);

		mydate = (Date) Calendar.getInstance().getTime();
		dt = mydate.toString();
		btngetPrice = (Button) findViewById(R.id.calcPrice);
		txtQuantity = (EditText) findViewById(R.id.quantity_edittext);

		txtTableNumber = (EditText) findViewById(R.id.editTableNumber);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			inv = extras.getString("invoice_number");
			nmbr = extras.getString("number");
			txtTableNumber.setText(extras.getString("number"));
			waiter_id = extras.getString("waiter_id");
			//Toast.makeText(getApplicationContext(), inv, Toast.LENGTH_LONG)
				//	.show();

		}

		// call to retrieve products
		new GetProducts().execute();
		// Hashmap for Prodcuts List
		productsList = new ArrayList<HashMap<String, String>>();

		spinner = (Spinner) findViewById(R.id.products_spinner);

		// Price EditText
		txtPrice = (EditText) findViewById(R.id.editPrice);
		txtTotalPrice = (EditText) findViewById(R.id.totalPrice);
		txtPrice.setText(TAG_PRICE);

		btngetPrice.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				calculate();

			}
		});

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {

				// TODO Auto-generated method stub

				theProductID = spinner.getSelectedItemId() + 1;
				txtTotalPrice.setText("");

				new GetPriceFromProduct().execute();

				//Toast.makeText(getApplicationContext(), "Selected Item",
						//Toast.LENGTH_SHORT).show();

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		/*
		 * priceSpinner = (Spinner) findViewById(R.id.quantity_spinner);
		 * ArrayAdapter<CharSequence> myadapter =
		 * ArrayAdapter.createFromResource( this, R.array.quantity_array,
		 * android.R.layout.simple_spinner_item); myadapter
		 * .setDropDownViewResource
		 * (android.R.layout.simple_spinner_dropdown_item);
		 * priceSpinner.setAdapter(myadapter);
		 */

		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#2E9AFE")));
	}

	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class GetProducts extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AddProduct.this);
			pDialog.setMessage("Getting Products...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_get_products2, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// products found
					// Getting Array of Products
					products = json.getJSONArray(TAG_PRODUCTS);

					// looping through All Products
					for (int i = 0; i < products.length(); i++) {
						JSONObject c = products.getJSONObject(i);

						// Storing each json item in variable
						String id = c.getString(TAG_PID);
						String name = c.getString(TAG_NAME);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_PID, id);
						map.put(TAG_NAME, name);

						// adding HashList to ArrayList
						productsList.add(map);
					}
				} else {
					// no students found
					// Launch Add New student Activity
					/*
					 * Intent i = new Intent(getApplicationContext(),
					 * AddStudent.class); // Closing all previous activities
					 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 * startActivity(i);
					 */
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into Spinner
					 * */

					ArrayList<String> list = new ArrayList<String>();

					try {

						for (int i = 0; i < products.length(); i++) {
							JSONObject c = products.getJSONObject(i);

							// Storing each json item in variable
							String id = c.getString(TAG_PID);
							String name = c.getString(TAG_NAME);
							String price = c.getString(TAG_PRICE);

							// list.add(id);
							list.add(name);

							txtPrice.setText(price);

						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

					ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
							getApplicationContext(),
							android.R.layout.simple_spinner_item, list);

					dataAdapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinner.setAdapter(dataAdapter);

				}
			});

		}

	}

	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class GetPriceFromProduct extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AddProduct.this);
			pDialog.setMessage("Getting Price...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {

			String productID = Long.toString(theProductID);

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("product_name", productID));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_get_price, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("Price: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {

					prices = json.getJSONArray(TAG_PRICES);

					// looping through All Products
					for (int i = 0; i < prices.length(); i++) {
						JSONObject c = prices.getJSONObject(i);

						// Storing each json item in variable
						product_price = c.getString(THE_PRICE);

						runOnUiThread(new Runnable() {
							public void run() {

							}
						});

					}

				} else {
					// no students found
					// Launch Add New student Activity
					/*
					 * Intent i = new Intent(getApplicationContext(),
					 * AddStudent.class); // Closing all previous activities
					 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 * startActivity(i);
					 */
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					txtPrice.setText(product_price);

				}
			});

		}

	}

	/**
	 * Background Async Task to Create new student
	 * */
	class NewProduct extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(AddProduct.this);
			pDialog.setMessage("Adding Product..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Adding Product
		 * */
		protected String doInBackground(String... args) {

			String invoice_no = "AAA123";
			String product_name = spinner.getSelectedItem().toString();
			String quantity = priceSpinner.getSelectedItem().toString();
			String price = txtPrice.getText().toString();

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("invoice_no", invoice_no));
			params.add(new BasicNameValuePair("product_name", product_name));
			params.add(new BasicNameValuePair("quantity", quantity));
			params.add(new BasicNameValuePair("price", price));

			// getting JSON Object
			// Note that create student url accepts POST method
			JSONObject json = jParser.makeHttpRequest(
					url_add_product_to_invoice, "POST", params);

			System.out.println("fIRST " + product_name);

			// check log cat for response
			Log.d("Create Response", json.toString());

			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// successfully created product
					Intent i = new Intent(getApplicationContext(),
							InvoiceConsumer.class);
					startActivity(i);

					// closing this screen
					finish();
				} else {
					// failed to create product
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once done
			pDialog.dismiss();
		}

	}

	private void calculate() {
		x = Double.parseDouble(txtQuantity.getText().toString());
		y = Double.parseDouble(txtPrice.getText().toString());
		z = x * y;
		txtTotalPrice.setText(Double.toString(z));
	}

	private void saveProduct() {

		DatabaseConnector dbConnector = new DatabaseConnector(this);

		// if (getIntent().getExtras() == null) {
		dbConnector.insertProduct(dt.toString(), txtTableNumber.getText()
				.toString(), waiter_id, spinner.getSelectedItem().toString(), txtQuantity
				.getText().toString(), txtTotalPrice.getText().toString());

		Toast.makeText(getApplicationContext(), "Invoice Updated",
				Toast.LENGTH_SHORT).show();
		// successfully created product
		Intent i = new Intent(getApplicationContext(), MyInvoice.class);

		i.putExtra("number", txtTableNumber.getText().toString());
		i.putExtra("waiter_id", waiter_id);

		startActivity(i);

		// closing this screen
		finish();

		// }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.confirm_product_menu, menu);
		// return true;
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.confirmItem:

			// new NewProduct().execute();
			saveProduct();

			return true;

		case R.id.Refresh:

			new GetProducts().execute();

			return true;

		case R.id.viewInvoice:

			Intent i = new Intent(getApplicationContext(), MyInvoice.class);
			startActivity(i);

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
