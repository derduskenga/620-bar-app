package com.sixtwenty.bar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockListActivity;

public class ViewInventory extends SherlockListActivity {

	ActionBar actionBar;
	EditText inputSearch;
	String dt;
	Date mydate;
	String invoice_no = "AAA123", inv, nmbr, waiter_id;
	ListAdapter adapter;
	ArrayList<HashMap<String, String>> invoiceList;
	// get products url
	private static String url_get_products = "http://10.0.2.2/vc/chapaa/get_products.php";
	private static String url_get_products2 = "http://www.padawan.site40.net/chapaa/get_products.php";
	// post to invoice
	private static String url_add_product_to_invoice = "http://10.0.2.2/vc/chapaa/add_product.php";

	private static String url_get_price = "http://www.padawan.site40.net/chapaa/get_price.php";
	private static String url_add_product_to_invoice2 = "http://www.padawan.site40.net/chapaa/add_product.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCTS = "products";
	private static final String TAG_PID = "pid";
	private static final String TAG_NAME = "name";
	private static final String TAG_PRICE = "price";
	private static final String THE_PRICE = "theprice";
	private static final String TAG_PRICES = "product_price";

	// products JSONArray
	JSONArray products = null;
	private ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();

	ArrayList<HashMap<String, String>> productsList;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header_add_product);
		setContentView(R.layout.all_invoice);
		

		inputSearch = (EditText) findViewById(R.id.inputSearch);
		inputSearch.addTextChangedListener(new TextWatcher() {
            
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
            	((SimpleAdapter) ViewInventory.this.adapter).getFilter().filter(cs);  
            	//((Filterable) MyOrders.this.invoiceList).getFilter().filter(cs);
            }
             
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
            }
             
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub                          
            }
        });

		// call to retrieve products
		new GetProducts().execute();
		

		// Initializing Action Bar
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#2E9AFE")));
	}

	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class GetProducts extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ViewInventory.this);
			pDialog.setMessage("Getting Products...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_get_products2, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// products found
					// Getting Array of Products
					products = json.getJSONArray(TAG_PRODUCTS);

					// looping through All Products
					for (int i = 0; i < products.length(); i++) {
						JSONObject c = products.getJSONObject(i);

						// Storing each json item in variable
						String id = c.getString(TAG_PID);
						String name = c.getString(TAG_NAME);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_PID, id);
						map.put(TAG_NAME, name);

						// adding HashList to ArrayList
						productsList.add(map);
					}
				} else {
					// no students found
					// Launch Add New student Activity
					/*
					 * Intent i = new Intent(getApplicationContext(),
					 * AddStudent.class); // Closing all previous activities
					 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 * startActivity(i);
					 */
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {

					/**
					 * Updating parsed JSON data into ListView
					 * */
					adapter = new SimpleAdapter(ViewInventory.this,
							invoiceList, R.layout.inventory_layout,
							new String[] { TAG_PID, TAG_NAME, TAG_PRICE, },
							new int[] { R.id.pid, R.id.actualRef,
									R.id.actualDate });
					// updating listview
					setListAdapter(adapter);

				}
			});

		}

	}

}
