package com.sixtwenty.bar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnActionExpandListener;
import com.actionbarsherlock.widget.SearchView;

public class MyOrders extends SherlockListActivity {

	String waiter_id;
	EditText inputSearch;
	ActionBar actionBar;
	ListAdapter adapter;
	private ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	ArrayList<HashMap<String, String>> invoiceList;
	private static String url_all_invoice = "http://10.0.2.2/vc/chapaa/get_all_invoice.php";
	private static String url_all_invoice2 = "http://www.padawan.site40.net/chapaa/get_waiter_orders.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCTS = "products";
	private static final String TAG_PID = "pid";
	private static final String TAG_ORDER_REF = "invoice";
	private static final String TAG_STATUS = "status";
	private static final String TAG_DATE = "invoice_date";
	private static final String TAG_NUMBER = "phone_number";
	private static final String TAG = null;

	// products JSONArray
	JSONArray products = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header_my_orders);
		setContentView(R.layout.all_invoice);

		inputSearch = (EditText) findViewById(R.id.inputSearch);
		inputSearch.addTextChangedListener(new TextWatcher() {
            
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
            	((SimpleAdapter) MyOrders.this.adapter).getFilter().filter(cs);  
            	//((Filterable) MyOrders.this.invoiceList).getFilter().filter(cs);
            }
             
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
            }
             
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub                          
            }
        });
		
		
		
		// Hashmap for ListView
		invoiceList = new ArrayList<HashMap<String, String>>();

		// Getting Waiter ID from Login Class
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			waiter_id = extras.getString("waiter_id");

		}

		// Loading Waiter Orders
		new LoadAllOrders().execute();

		// Get listview
		ListView lv = getListView();

		// on seleting single product
		// launching Edit Product Screen
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				String invoice_number = ((TextView) view
						.findViewById(R.id.actualRef)).getText().toString();

				// Starting new intent
				Intent in = new Intent(getApplicationContext(),
						ConsumerOrders.class);
				// sending pid to next activity
				in.putExtra("theinvoice", invoice_number);
				System.out.println(invoice_number);

				// starting new activity and expecting some response back
				startActivityForResult(in, 100);
			}
		});

		// Initializing Action Bar
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#2E9AFE")));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.paybill_menu, menu);
		
		// Create the search view
		//SearchView searchView = new SearchView(getSupportActionBar()
		//		.getThemedContext());
		//searchView.setQueryHint("Filter Invoices");

		/*
		 * menu.add("Search") .setIcon(R.drawable.abs__ic_search)
		 * .setActionView(searchView)
		 * .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
		 * MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		 */

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.addOrder:

			Intent in = new Intent(getApplicationContext(), AddProduct.class);

			in.putExtra("waiter_id", waiter_id);

			startActivity(in);

			return true;
			
		case R.id.Refresh:

			new LoadAllOrders().execute();

			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Background Async Task to Load all invoices by making HTTP Request
	 * */
	class LoadAllOrders extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MyOrders.this);
			pDialog.setMessage("Getting Orders. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("waiter_id", waiter_id));

			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(url_all_invoice2, "GET",
					params);

			// Check your log cat for JSON reponse
			Log.d("All Invoices: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// products found
					// Getting Array of Products
					products = json.getJSONArray(TAG_PRODUCTS);

					// looping through All Products
					for (int i = 0; i < products.length(); i++) {
						JSONObject c = products.getJSONObject(i);

						// Storing each json item in variable
						String id = c.getString(TAG_PID);
						String status = c.getString(TAG_STATUS);
						String number = c.getString(TAG_NUMBER);
						String invoice_no = c.getString(TAG_ORDER_REF);
						String date = c.getString(TAG_DATE);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_PID, id);
						map.put(TAG_STATUS, status);
						map.put(TAG_NUMBER, number);
						map.put(TAG_ORDER_REF, invoice_no);
						map.put(TAG_DATE, date);

						// adding HashList to ArrayList
						invoiceList.add(map);
					}
				} else {
					// no students found
					// Launch Add New student Activity
					// Intent i = new Intent(getApplicationContext(),
					// AddStudent.class);
					// Closing all previous activities
					// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					// startActivity(i);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */
					adapter = new SimpleAdapter(MyOrders.this,
							invoiceList, R.layout.paybill_transaction,
							new String[] { TAG_PID, TAG_ORDER_REF, TAG_STATUS,
									TAG_DATE, TAG_NUMBER }, new int[] {
									R.id.pid, R.id.actualRef,
									R.id.actualStatus, R.id.actualDate,
									R.id.actualNumber });
					// updating listview
					setListAdapter(adapter);

				}
			});

		}

	}
}
