package com.sixtwenty.bar;

import java.util.ArrayList;
import java.util.List;

import javax.sql.RowSetWriter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.hewani.handlers.DatabaseConnector;

public class MyInvoice extends SherlockListActivity {

	private ListView studentListView;
	private CursorAdapter conAdapter;
	ActionBar actionBar;
	private ProgressDialog pDialog;
	ProgressDialog syncing;
	Context dialogContext;
	Handler handle;
	String result, feedback, fullresult= "";
	TextView txtTotal, txtCharge, txtSubtotal;
	String ph_number, inv_number;
	
	public static final String ROW_ID = "_id";
	private Long rowID;
	
	ShemJSONParser ShemParser = new ShemJSONParser();

	// post to invoice
	private static String url_add_product_to_invoice = "http://10.0.2.2/vc/chapaa/add_product.php";
	private static String url_add_product_to_invoice2 = "http://www.padawan.site40.net/chapaa/add_product2.php";
	String product_id, product_name, price, quantity, invoice_no;
	// JSON Node names
	private static final String TAG_SUCCESS = "success";

	public static String[] product_array;
	String waiter_id;

	JSONParser jParser = new JSONParser();

	@SuppressWarnings("deprecation")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header_my_invoice);
		
		studentListView = getListView();
		studentListView.setOnItemLongClickListener(viewConListener);
		
		dialogContext = new ContextThemeWrapper(this, R.style.AppBaseTheme);
		// retrieving invoice from phone
		new GetProducts().execute((Object[]) null);

		// map each name to a TextView
		String[] from = new String[] { "product_name", "price", "quantity" };
		int[] to = new int[] { R.id.title, R.id.duration, R.id.quantity };
		conAdapter = new SimpleCursorAdapter(MyInvoice.this,
				R.layout.products_list, null, from, to);
		setListAdapter(conAdapter); // set adapter

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			inv_number = extras.getString("invoice_number");
			ph_number = extras.getString("number");
			waiter_id = extras.getString("waiter_id");

		}
		
		actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#2E9AFE")));

	}

	@Override
	protected void onResume() {
		super.onResume();
		new GetProducts().execute((Object[]) null);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onStop() {
		Cursor cursor = conAdapter.getCursor();

		if (cursor != null)
			cursor.deactivate();

		conAdapter.changeCursor(null);
		super.onStop();
	}

	private class GetProducts extends AsyncTask<Object, Object, Cursor> {
		DatabaseConnector dbConnector = new DatabaseConnector(MyInvoice.this);

		@Override
		protected Cursor doInBackground(Object... params) {
			dbConnector.open();
			return dbConnector.getAllProducts();
		}

		@Override
		protected void onPostExecute(Cursor result) {
			conAdapter.changeCursor(result); // set the adapter's Cursor
			dbConnector.close();
		}
	}
	
	
	OnItemLongClickListener viewConListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			
			//Toast.makeText(MyInvoice.this, "Im gonna delete you", Toast.LENGTH_SHORT).show();
			deleteProduct();
			return false;
		}

	};
	
	private void deleteProduct() {

		AlertDialog.Builder alert = new AlertDialog.Builder(MyInvoice.this);

		alert.setTitle(R.string.confirmTitle);
		alert.setMessage(R.string.confirmMessage);

		alert.setPositiveButton(R.string.delete_btn,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int button) {
						
						final DatabaseConnector dbConnector = new DatabaseConnector(
								MyInvoice.this);

						AsyncTask<Long, Object, Object> deleteTask = new AsyncTask<Long, Object, Object>() {
							@Override
							protected Object doInBackground(Long... params) {
								dbConnector.deleteProduct(params[0]);
								return null;
							}

							@Override
							protected void onPostExecute(Object result) {
								//finish();
							}
						};

						deleteTask.execute(new Long[] { rowID });
					}
				});

		alert.setNegativeButton(R.string.cancel_btn, null).show();
	}




	public void SyncToDB() {

		syncing = ProgressDialog.show(dialogContext, "", "Sync in Progress...",
				true, false);

		Thread save = new Thread() {
			public void run() {
				Looper.prepare();

				// Opening database
				DatabaseConnector dbConnector = new DatabaseConnector(
						MyInvoice.this);

				dbConnector.open();

				Cursor cursor = dbConnector.getAllProducts();

				JSONArray resultSet = new JSONArray();

				cursor.moveToFirst();
				while (cursor.isAfterLast() == false) {

					int totalColumn = cursor.getColumnCount();
					JSONObject rowObject = new JSONObject();

					for (int i = 0; i < totalColumn; i++) {
						if (cursor.getColumnName(i) != null) {

							try {

								if (cursor.getString(i) != null) {
									Log.d("TAG_NAME", cursor.getString(i));
									rowObject.put(cursor.getColumnName(i),
											cursor.getString(i));
								} else {
									rowObject.put(cursor.getColumnName(i), "");
								}
							} catch (Exception e) {
								Log.d("TAG_NAME", e.getMessage());
							}

						}
					}

					resultSet.put(rowObject);
					cursor.moveToNext();

				}
				Log.d("TAG_NAME", resultSet.toString());

				dbConnector.close();

				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("product_array", resultSet
						.toString())); // resultset
										// is
										// the
										// JSON
										// input

				// getting JSON Object
				// Note that create student url accepts POST method
				JSONObject json = jParser.makeHttpRequest(
						url_add_product_to_invoice2, "POST", params);

				// check log cat for response
				Log.d("Create Response", json.toString());

				// check for success tag
				try {
					int success = json.getInt(TAG_SUCCESS);

					if (success == 1) {
						// successfully created product
						// clearing the invoice after input
						new ClearInvoice().execute((Object[]) null);

						// Send Order Ref to next activity
/*
						Toast.makeText(getApplicationContext(),
								"Invoice Created", Toast.LENGTH_SHORT).show();*/
						Intent in = new Intent(getApplicationContext(),
								InvoiceSent.class);
						
						
						in.putExtra("number", ph_number);
						in.putExtra("invoice", inv_number);
						
						startActivity(in);

						// closing this screen
						finish();
					} else {
						// failed to create product
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}// end runnable

		};// end thread

		save.start();

		handle = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (result.charAt(0) == 's') {

					syncing.dismiss();

					Toast.makeText(getApplicationContext(), "Uploaded!",
							Toast.LENGTH_LONG).show();
				}
				syncing.dismiss();
			}

		};// end handler
		Looper.loop();

	}

	/**
	 * Background Async Task to Create new student
	 * */
	class SendInvoice extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MyInvoice.this);
			pDialog.setMessage("Creating and Uploading Invoice..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * Adding Product
		 * */
		/*
		 * (non-Javadoc)
		 * 
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		protected String doInBackground(String... args) {

			// Opening database
			DatabaseConnector dbConnector = new DatabaseConnector(
					MyInvoice.this);

			dbConnector.open();

			Cursor cursor = dbConnector.getAllProducts();

			JSONArray resultSet = new JSONArray();

			cursor.moveToFirst();
			while (cursor.isAfterLast() == false) {

				int totalColumn = cursor.getColumnCount();
				JSONObject rowObject = new JSONObject();

				for (int i = 0; i < totalColumn; i++) {
					if (cursor.getColumnName(i) != null) {

						try {

							if (cursor.getString(i) != null) {
								Log.d("TAG_NAME", cursor.getString(i));
								rowObject.put(cursor.getColumnName(i),
										cursor.getString(i));
							} else {
								rowObject.put(cursor.getColumnName(i), "");
							}
						} catch (Exception e) {
							Log.d("TAG_NAME", e.getMessage());
						}

					}
				}

				resultSet.put(rowObject);
				cursor.moveToNext();

			}
			
			Log.d("TAG_NAME", resultSet.toString());

			dbConnector.close();
			
			fullresult = resultSet.toString();

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("product_array", fullresult)); // resultset is the JSON input

			
			//Log.i("tagconvertstr", "["+fullresult+"]");
			// getting JSON Object
			// Note that create student url accepts POST method
			
			JSONObject json = jParser.makeHttpRequest(url_add_product_to_invoice2, "POST", params);

			// check log cat for response
			Log.d("Create Response", json.toString());

			// check for success tag
			try {
				int success = json.getInt(TAG_SUCCESS);

				if (success == 4) {
					// successfully created product

					// clearing the invoice after input
					new ClearInvoice().execute((Object[]) null);

					// Send Order Ref to next activity
/*
					Toast.makeText(getApplicationContext(),
							"Invoice Created", Toast.LENGTH_SHORT).show();*/
					Intent in = new Intent(getApplicationContext(),
							MyOrders.class);
					
					
					in.putExtra("waiter_id", waiter_id);
					//in.putExtra("invoice", inv_number);
					
					startActivity(in);


					// closing this screen
					finish();
				} else {
					// failed to create product
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;

		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once done
			pDialog.dismiss();
		}

	}

	private class ClearInvoice extends AsyncTask<Object, Object, Cursor> {
		DatabaseConnector dbConnector = new DatabaseConnector(MyInvoice.this);

		@Override
		protected Cursor doInBackground(Object... params) {
			dbConnector.open();
			return dbConnector.deleteAll();
		}

		@Override
		protected void onPostExecute(Cursor result) {
			conAdapter.changeCursor(result); // set the adapter's Cursor
			dbConnector.close();
		}
	}
	
	/*private class DeleteItem extends AsyncTask<Object, Object, Cursor> {
		DatabaseConnector dbConnector = new DatabaseConnector(MyInvoice.this);

		@Override
		protected Cursor doInBackground(Object... params) {
			dbConnector.open();
			dbConnector.deleteProduct(params[0]);
		}

		@Override
		protected void onPostExecute(Cursor result) {
			conAdapter.changeCursor(result); // set the adapter's Cursor
			dbConnector.close();
		}
	}
	*/
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_action_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.deleteItem:

			new ClearInvoice().execute((Object[]) null);

			return true;

		case R.id.SyncItems:

			new SendInvoice().execute();
			//SyncToDB();
			return true;

		case R.id.AddItems:

			Intent in = new Intent(getApplicationContext(), AddProduct.class);

			in.putExtra("number", ph_number);
			in.putExtra("invoice_number", inv_number);

			startActivity(in);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}