package com.sixtwenty.bar;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class HomeActivity extends SlidingBaseActivity {

	ActionBar actionBar;
	Button btninvoice, btnpaybill, btnbuygoods;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header);
		setContentView(R.layout.home_screen);

		btninvoice = (Button) findViewById(R.id.btninvoice);
		btnpaybill = (Button) findViewById(R.id.btnpaybill);
		btnbuygoods = (Button) findViewById(R.id.btnbuygoods);

		btninvoice.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						MyOrders.class);
				startActivity(i);

			}
		});

		btnpaybill.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						PayBillTransactions.class);
				startActivity(i);

			}
		});
		

		btnbuygoods.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				

			}
		});

		actionBar = getSupportActionBar();
		//actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#2E9AFE")));
		;

	}

}
