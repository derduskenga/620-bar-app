package com.hewani.handlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DatabaseConnector {

	// Database Version
	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "Bars";
	private SQLiteDatabase database;
	private DatabaseOpenHelper dbOpenHelper;
	
	// products JSONArray
	JSONArray products = null;

	public DatabaseConnector(Context context) {
		dbOpenHelper = new DatabaseOpenHelper(context, DB_NAME, null,
				DB_VERSION);
	}

	public void open() throws SQLException {
		// open database in reading/writing mode
		database = dbOpenHelper.getWritableDatabase();
	}

	public void close() {
		if (database != null)
			database.close();
	}

	// Adding new invoice
	public void insertProduct(String current_date , String table_number, String waiter_id, String product_name, String quantity, String price) {
		ContentValues newCon = new ContentValues();
		
		newCon.put("invoice_date", current_date);
		newCon.put("table_number", table_number);
		newCon.put("waiter_id", waiter_id);
		newCon.put("product_name", product_name);
		newCon.put("quantity", quantity);
		newCon.put("price", price);
		

		open();
		database.insert("orders", null, newCon);
		close();
	}

	// Updating Product details
	public void updateProduct(long id, String firstname, String lastname,
			String gender, int lang1, int lang2, int lang3) {
		
		
		ContentValues editCon = new ContentValues();
		editCon.put("fname", firstname);
		editCon.put("lname", lastname);
		editCon.put("gender", gender);
		editCon.put("lang1", lang1);
		editCon.put("lang2", lang2);
		editCon.put("lang3", lang3);

		open();
		database.update("invoice", editCon, "_id=" + id, null);
		close();
	}

	public Cursor getAllProducts() {
		// return database.query("invoice", new String[] { "_id",
		// "product_name", "price" ,"quantity"},
		// null, null, null, null, "product_name");

		Cursor cursor = database
				.rawQuery(
						" SELECT _id, product_name, waiter_id, table_number, price ,quantity FROM  orders ORDER BY _id",
						null);
		
		

		
		
		return cursor;
		//return resultSet;

	}
	
	public Cursor getSum() {
		
		Cursor cursor = database
				.rawQuery(
						" SELECT price  from orders",
						null);
		
		/*int totalColumn = cursor.getColumnCount();
		String total = "0";
		
		for (int i = 0; i < totalColumn ; i++ ) {
			if (cursor.getColumnName(i) != null) {
				
				String mytotal = cursor.getString(i);
				total = total + mytotal;
				
			}
		}*/
	
		
		return cursor;
	}

	public Cursor getOneProduct(long id) {
		return database.query("orders", null, "_id=" + id, null, null, null,
				null);
	}

	public void deleteProduct(long id) {
		open();
		database.delete("orders", "_id=" + id, null);
		close();
	}
	
	public Cursor deleteAll() {
		
		
		database.delete("orders", null, null);
		return null;
		
		
	}

}
