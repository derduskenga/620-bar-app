package com.sixtwenty.bar;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;

import com.hewani.fragments.HewaniMenuFragment;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingFragmentActivity;


public class SlidingBaseActivity extends SlidingFragmentActivity {
	protected ListFragment mFrag;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header);
		setContentView(R.layout.home_screen);

		// set the Behind View
		setBehindContentView(R.layout.menu_frame);
		getSlidingMenu().setBehindOffset(100);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.menu_frame, new HewaniMenuFragment()).commit();

		// Customizing Sliding Menu
		SlidingMenu menu;
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.LEFT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		menu.setShadowWidth(5);
		menu.setShadowWidthRes(R.dimen.shadow_width);
		menu.setShadowDrawable(R.drawable.shadow);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		menu.setBehindWidth(250);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}

}
