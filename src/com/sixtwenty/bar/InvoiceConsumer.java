package com.sixtwenty.bar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class InvoiceConsumer extends SherlockActivity {

	ActionBar actionBar;
	EditText OrderRef, PhoneNumber;
	TextView txtdate;
	Date mydate;
	ListView list;
	String pid = "123", dt;
	AlertDialogManager alert = new AlertDialogManager();

	// Progress Dialog
	private ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	ArrayList<HashMap<String, String>> productsList;
	private static String url_get_products_from_invoice = "http://10.0.2.2/vc/chapaa/get_products_invoice.php";
	private static String url_get_products_from_invoice2 = "http://www.padawan.site40.net/chapaa/get_products_invoice.php";

	// JSON Node names
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_PRODUCTS = "products";
	private static final String TAG_PID = "pid";
	private static final String TAG_NAME = "product";

	// products JSONArray
	JSONArray products = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.header_invoice_consumer);
		setContentView(R.layout.invoice);

		txtdate = (TextView) findViewById(R.id.txtMyDate);
		// list = (ListView) findViewById(R.id.list);

		mydate = (Date) Calendar.getInstance().getTime();
		dt = mydate.toString();
		txtdate.setText(dt.toString());

		OrderRef = (EditText) findViewById(R.id.editTextOrderRef);
		
		OrderRef.setText("");

		PhoneNumber = (EditText) findViewById(R.id.editConsumer);

		// Retrieve first product
		// new getInvoiceProducts().execute();

		// Initializing Action Bar
		actionBar = getSupportActionBar();
		//actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#2E9AFE")));
		;

	}

	/**
	 * Background Async Task to Load all product by making HTTP Request
	 * */
	class getInvoiceProducts extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(InvoiceConsumer.this);
			pDialog.setMessage("Getting Invoice. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("pid", pid));
			// getting JSON string from URL
			JSONObject json = jParser.makeHttpRequest(
					url_get_products_from_invoice2, "GET", params);

			// Check your log cat for JSON reponse
			Log.d("All Products: ", json.toString());

			try {
				// Checking for SUCCESS TAG
				int success = json.getInt(TAG_SUCCESS);

				if (success == 1) {
					// products found
					// Getting Array of Products
					products = json.getJSONArray(TAG_PRODUCTS);

					// looping through All Products
					for (int i = 0; i < products.length(); i++) {
						JSONObject c = products.getJSONObject(i);

						// Storing each json item in variable
						String id = c.getString(TAG_PID);
						String name = c.getString(TAG_NAME);

						// creating new HashMap
						HashMap<String, String> map = new HashMap<String, String>();

						// adding each child node to HashMap key => value
						map.put(TAG_PID, id);
						map.put(TAG_NAME, name);

						// adding HashList to ArrayList
						productsList.add(map);
					}
				} else {
					// no students found
					// Launch Add New student Activity
					/*
					 * Intent i = new Intent(getApplicationContext(),
					 * AddStudent.class); // Closing all previous activities
					 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 * startActivity(i);
					 */
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after getting all products
			pDialog.dismiss();
			// updating UI from Background Thread
			runOnUiThread(new Runnable() {
				public void run() {
					/**
					 * Updating parsed JSON data into ListView
					 * */

					ListAdapter adapter = new SimpleAdapter(
							InvoiceConsumer.this, productsList,
							R.layout.products_list, new String[] { TAG_PID,
									TAG_NAME }, new int[] { R.id.pid,
									R.id.title });
					list.setAdapter(adapter);

				}
			});

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// menu.add(R.id.editItem).setIcon(android.R.drawable.ic_menu_edit)
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		// menu.add(R.id.deleteItem).setIcon(android.R.drawable.ic_menu_delete)
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.add_product_menu, menu);
		// return true;
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.addItem:

			String number = PhoneNumber.getText().toString();
			String invoice_number = OrderRef.getText().toString();
			
			if (number.trim().length() > 0 && invoice_number.trim().length() > 0) {
				Intent addproduct = new Intent(this, AddProduct.class);

				addproduct.putExtra("number", PhoneNumber.getText().toString());
				addproduct.putExtra("invoice_number", OrderRef.getText().toString());

				startActivity(addproduct);

			} else {
				alert.showAlertDialog(InvoiceConsumer.this, "Empty Field",
						"Please enter a valid Phone Number and Invoice No.", false);

			}
			return true;

		case R.id.view_invoice:
			Intent inv = new Intent(this, MyInvoice.class);
			startActivity(inv);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
